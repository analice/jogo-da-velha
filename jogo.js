const j1 = "Monica";
const j2 = "Cebolinha";
var playTime = j1;
var gameOver = false;

Espacos();

function Espacos(){

	var espacos = document.getElementsByClassName("espaco");
	for (var i = 0; i < espacos.length; i++) {

		espacos[i].addEventListener("click", function(){

			if (gameOver) {return;}

			if(this.getElementsByTagName("img").length == 0){

				if (playTime == j1) {
					this.innerHTML = "<img src='0.png' border='0'>";
					this.setAttribute("jogada", j1);
					$("#mon").html("<h1>"+j2+"</h1>");
					playTime = j2;

				}else{
					this.innerHTML = "<img src='1.png' border='0'>";
					this.setAttribute("jogada", j2);
					$("#mon").html("<h1>"+j1+"</h1>");
					playTime = j1;
				}
				quemGanhou();

			}

		});
	}
}

function quemGanhou(){

	var a = document.getElementById("a").getAttribute("jogada");
	var b = document.getElementById("b").getAttribute("jogada");
	var c = document.getElementById("c").getAttribute("jogada");
	var d = document.getElementById("d").getAttribute("jogada");
	var e = document.getElementById("e").getAttribute("jogada");
	var f = document.getElementById("f").getAttribute("jogada");
	var g = document.getElementById("g").getAttribute("jogada");
	var h = document.getElementById("h").getAttribute("jogada");
	var i = document.getElementById("i").getAttribute("jogada");

	var v = "";

	if(
		((a == d && a == g) || 
		(a == b && a == c) || 
		(a==e && a == i) && a != "")
		){v = a;

	}else if(
		(e == d && e == f && e !="") ||
	 	(e==b && e==h && e !="") || 
	 	(e==c && e==g && e!="")
	 	){v = e;

	}else if(
		((i==h && i==g) ||
		(i==c && i == f) && i != "")
		){v = i;

	}else if(
		(a != null && a != 0) &&
		(b != null && b != 0) &&
		(c != null && c != 0) &&
		(d != null && d != 0) &&
		(e != null && e != 0) &&
		(f != null && f != 0) &&
		(g != null && g != 0) &&
		(h != null && h != 0) &&
		(i != null && i != 0)
		){$("#venceu").html("<h1>Empate!</h1>");
	}

	if (v != ""){
		gameOver = true;
		$("#venceu").html("<h1>O ganhador foi o(a) " + v + ".</h1>");
	}
}

